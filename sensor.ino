double getWaterLevel(int DEPTH_SENSOR_TRANSMITTER_PIN, int DEPTH_SENSOR_RECEIVER_PIN)
{
  double distance;

  // changing state of SOUND_SENSOR_TRANSMITTER_PIN to LOW for getting a clean HIGH pulse
  digitalWrite(DEPTH_SENSOR_TRANSMITTER_PIN, LOW);
  delay(20);
  digitalWrite(DEPTH_SENSOR_TRANSMITTER_PIN, HIGH);
  delay(200);
  digitalWrite(DEPTH_SENSOR_TRANSMITTER_PIN, LOW);

  // Checking pulse duration
  // calculating distance as d=(v/2)*t
  distance = (pulseIn(DEPTH_SENSOR_RECEIVER_PIN, HIGH) / 2) / 29.1;
  Serial.printf("[INFO] Depth: %f cm \n", distance);
  delay(100);
  return distance;
}